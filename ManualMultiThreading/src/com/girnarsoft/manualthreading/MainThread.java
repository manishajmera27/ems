package com.girnarsoft.manualthreading;

import java.util.LinkedList;
import java.util.Queue;

public class MainThread {
	static Queue <Thread> queueThread = new LinkedList<>(); 
	static Thread myThread[] = new MyThread[100];
	public static void main(String[] args) {
				for (int i = 0; i < 100; i++) {
					myThread[i] = new MyThread();
					queueThread.add(myThread[i]);
				}
		try {
		     Thread.sleep(1000);
		} catch (InterruptedException e) {
		      System.out.println("Main thread Interrupted");
		}	
	}
}
