package com.girnarsoft.studentresult;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class StudentValidate extends ActionForm{
	
	   private static final long serialVersionUID = 7760412395861134002L;
	    private String name = null;
	    private String dob = null;
	    @Override
	    public ActionErrors validate(ActionMapping mapping,
	     HttpServletRequest request) {
	        ActionErrors errors = new ActionErrors();
	        if(this.name == null || this.name.trim().equals(""))
	            errors.add("name", new ActionMessage("errors.required","Name"));
	        if(this.dob == null || this.dob.trim().equals(""))
	            errors.add("dob", new ActionMessage("errors.required","dob"));
	        return errors;
	    }

	    @Override
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	        this.name = null;
	        this.dob = null;
	        super.reset(mapping, request);
	    }

}
