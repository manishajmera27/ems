package com.girnarsoft.studentresult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class StudentResult extends Action {
	@Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws Exception {
	if(getErrors(request) == null ||getErrors(request).size() == 0) {
		String stdResult[][] = {{"Suresh","89%"},{"Ramesh","99%"},
				{"Iqbal","95%"},{"Ram","65%"} };
		String stdName = request.getParameter("name");
		for(int i=0;i<4;i++)
		{
			if(stdResult[i][0].equals(stdName))
			{
				request.setAttribute("result", stdResult[i][1]);
		        return mapping.findForward("result");

			}
		}
		
       
		}
			 return mapping.findForward("fail");
		

    }
	

}
