package com.first;

public class Car extends Vehicle{
	private int noOfSeats;
	private String brand;
	Car(String vehicalNum,int model,String brand,int noOfSeats)
	{
		super(vehicalNum,model);
		this.brand= brand;
		this.noOfSeats = noOfSeats;
	}
	public void display()
	{
		
		super.display();
		System.out.println("Type :"+ brand);
	}
	public void numberOfSeats() {
		System.out.println("NumberOfSeats :" + noOfSeats);
	}

}
