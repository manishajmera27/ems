package com.girnarsoft.exception;
/**
 * Class for handling illegal string which extend ILLEGAL ARgument Exception
 * 
 * @author gspl
 *
 */
public class UncheckException extends IllegalArgumentException {
	public UncheckException(String s) {
		super(s);

	}

}
