package com.girnarsoft.exception;

import java.util.Scanner;

/**
 * main excecutor class for taking input string and check whether it cantains
 * illegal format and gave exceptions
 * 
 * @author gspl
 *
 */
public class MainException {
	/*
	 * main exception class
	 */
	public static void main(String[] args) {
		// Scanner for taking input from user
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String to be handled");
		// user inputed string
		String str = sc.nextLine();
		// method call to check valid string
		checkString(str);
	}
	/*
	 * string chceker method which checks string doesn't contain number and space
	 * and gave exception;
	 */

	public static void checkString(String str) {
		// regex string to chcek string
		String regex = "^[a-zA-Z]+$";
		if (!str.matches(regex)) {
			throw new UncheckException("illegal String " + "and handled.");
		} else {
			System.out.println("String format is wrong");
		}

	}
}
