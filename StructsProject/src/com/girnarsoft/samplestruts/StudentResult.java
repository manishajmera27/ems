package com.girnarsoft.samplestruts;

import java.sql.Connection;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 * Action class to test whether student in databse or not
 * @author gspl
 *
 */
public class StudentResult extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JdbcConnection dbConnection = new JdbcConnection();
		Connection conn = dbConnection.getDbTxConnection();
		DataBaseOperations dbOperation = new DataBaseOperations();
		System.out.println(conn);
		dbOperation.insertStd(conn,"Satyendra", "Ram Prakash ","45%");
		return mapping.findForward("result");
		/*Map studentResult = new HashMap<>();//student name with result
		studentResult.put("suresh", "89%");
		studentResult.put("ramesh", "99%");
		studentResult.put("iqbal", "95%");
		studentResult.put("ram", "65%");
		studentResult.put("manish", "75%");
		studentResult.put("syam", "65%");
		String stdName = request.getParameter("name");
		stdName = stdName.toLowerCase();
		if (studentResult.containsKey(stdName)) {
			request.setAttribute("studentName", stdName);
			request.setAttribute("result", studentResult.get(stdName));
			return mapping.findForward("result");

		} else {
			request.setAttribute("error", "Not in Database");
			return mapping.findForward("fail");
		}*/

	}

}
