package com.girnarsoft.samplestruts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
public class JdbcConnection {
	//final scanner to take users input
		//variable to store newly created employee id
		//object of checker class which validate the valid inputs
		//JDBC_Driver 
		public static String JDBC_DRIVER;
		//database path
		public  String TARGET_DB_URL;
		//database username 
		public  String USER;
		//database password
		public  String PASS;
		//database connection object
		public  Connection CONN = null;
		//resource bundle to fetch constant data of database
		public ResourceBundle bundle = null;
		/*
		 * Initialization of database
		 */
		/*
		 * creation of database connection
		 */
		public Connection getDbTxConnection() {
			Connection conn = null;
			bundle = ResourceBundle.getBundle("db");
			JDBC_DRIVER = bundle.getString("JDBC_DRIVER");
			TARGET_DB_URL = bundle.getString("TARGET_DB_URL");
			USER = bundle.getString("USER");
			PASS = bundle.getString("PASS");
			System.out.println(USER);
			System.out.println(PASS);
			try {
				
				Class.forName(JDBC_DRIVER);
				System.out.println("Connecting to database... ");
				conn = DriverManager.getConnection(TARGET_DB_URL, USER,PASS);		
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return conn;
		}

}
