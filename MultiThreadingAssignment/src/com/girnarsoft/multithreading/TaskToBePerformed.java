package com.girnarsoft.multithreading;

import java.util.Random;

/**
 * 
 * @author gspl Task that all the threads perform one by one
 */
public class TaskToBePerformed {
	// random number created
	Random rand = new Random();

	/*
	 * sum of two random numbers
	 */
	public int sum() {
		return rand.nextInt(100) + rand.nextInt(100);
	}

	/*
	 * multiply two random number
	 */
	public int multiply() {
		return rand.nextInt(100) * rand.nextInt(100);
	}

	/*
	 * subtraction of two random number
	 */
	public int subtract() {
		return rand.nextInt(100) - rand.nextInt(100);
	}

}
