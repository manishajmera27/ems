package com.girnarsoft.multithreading;

import java.util.Random;

/**
 * thread class which inherit all the properties of thread main class and a run
 * method which runs the thread.
 * 
 * @author gspl
 *
 */
public class MyThread extends Thread {
	// object of task class that to be performed
	TaskToBePerformed task = new TaskToBePerformed();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run() run method which overrides the default run
	 * properties of thread run method
	 */
	@Override
	public void run() {

		System.out.println("Thread Name : " + Thread.currentThread().getName());
		try {
			// sleeps thread for 1000ms
			Thread.sleep(1000);
			// random number generator to assign random task to thread
			Random rand = new Random();
			int randomNumber = rand.nextInt(3);
			// condition of random number to perform task on the basis of random number
			if (randomNumber == 0) {
				System.out.println("Thread performing task   " + Thread.currentThread().getName()
						+ " Sum Of two random number  " + task.sum());
			} else if (randomNumber == 1) {
				{
					System.out.println("Thread performing task   " + Thread.currentThread().getName()
							+ " Multiply Of two random number  " + task.multiply());
				}

			}

			else {
				System.out.println("Thread performing task   " + Thread.currentThread().getName()
						+ " Subtraction Of two random number " + task.subtract());
			}
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
		}
	}

}
