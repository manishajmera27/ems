package com.girnarsoft.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author gspl Main executor class where all the threads are put into queue and
 *         from that queue we take 10 threads at a time and perform their tasks
 *
 */

public class MainThread extends Thread {
	// thread queue
	static Thread myThread[] = new MyThread[100];

	/*
	 * main class
	 */
	public static void main(String[] args) {

		// pool of 10 threads
		ExecutorService pool = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 100; i++) {
			myThread[i] = new MyThread();
			pool.execute(myThread[i]);
		}
		// sleep main thread
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.out.println("Main thread Interrupted");
		}
		pool.shutdown();

	}

}
