package com.girnarsoft.example.jdbc;

public class Constants {

	public static interface Queries {
		public static final String INSERT_ADDRESS = "INSERT INTO `example`.`address` ("
				+"  `add1`,"
				+"  `add2`,"
				+"  `city`,"
				+"  `created_at`,"
				+"  `updated_at`"
				+") "
				+"VALUES"
				+"  ("
				+"    ?,"
				+"    ?,"
				+"    ?,"
				+"   NOW(),"
				+"    NOW()"
				+"  )";
		public static final String INSERT_EMPLOYEE = "INSERT INTO `example`.`employee` ("
				+"  `name`,"
				+"  `date_of_joining`,"
				+"  `created_at`,"
				+"  `updated_at`,"
				+"  `dept_id`,"
				+"  `address_id`,"
				+"  `salary`"
				+") "
				+"VALUES"
				+"  ("
				+"    ?,"
				+"    ?,"
				+"    NOW(),"
				+"    NOW(),"
				+"    ?,"
				+"    ?,"
				+"    ?"
				+"  ) ";
		public static final String DEPT_HAV_NO_EMPLOYEE="select department.name as dept_name  from department left join employee on "
				+ "employee.dept_id= department.dept_id where employee.emp_id is null;";
		public static final String DEPT_HAV_EMPLOYEE="select department.name as dept_name  from department left join employee on employee.dept_id= department.dept_id where employee.emp_id is not null" ;
		public static final String UPDATE_EMPLOYEE_SALARY = "update employee " + "set salary = if(dept_id=7,salary+salary*0.1,if(dept_id=4,salary+salary*0.1,salary));";
		public static final String EMPLOYEE_OF_PARTICULAR_DEPT = "select name,dept_id,salary from employee where dept_id = 7 || dept_id=4";
		public static final String COUNT_EMPLOYEE_OF_DEPT = "select department.name,count(employee.emp_id) from department inner join employee on "
				+ "department.dept_id = employee.dept_id group by department.dept_id;";
		public static final String EMP_OF_SAME_CITY = "SELECT employee.name,department.name,address.city FROM department INNER JOIN employee ON employee.dept_id=department.dept_id INNER JOIN address  ON address.id=employee.address_id;";
	}
	
}
