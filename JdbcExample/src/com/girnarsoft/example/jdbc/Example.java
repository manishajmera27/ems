package com.girnarsoft.example.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Example {

	public static String JDBC_DRIVER ;
	public static String TARGET_DB_URL;
	public static String OUTPUT_FOLDER_PATH ;
	public static String INFORMATION_DB_URL ;
	public static String USER ;
	public static String PASS ;
	static Connection connection = null;
	static Scanner sc = new Scanner(System.in);
	public static ResourceBundle bundle = null;
	 static {
		 
		 bundle=  ResourceBundle.getBundle("db");
		 JDBC_DRIVER = bundle.getString("JDBC_DRIVER");
		 TARGET_DB_URL = bundle.getString("TARGET_DB_URL");
		 USER = bundle.getString("USER");
		 PASS = bundle.getString("PASS");
		 
		  try{
			    
				connection = getDbTxConnection(TARGET_DB_URL, USER, PASS);
			    }catch(Exception e){
			    	e.printStackTrace();
			    }
	 }
	
	public static Connection getDbTxConnection(String dbUrl, String username, String password){
		 Connection conn = null;
		 try{
		      Class.forName(JDBC_DRIVER);
		      System.out.println("Connecting to database... ");
		      conn = DriverManager.getConnection(dbUrl,username,password);
		     }catch(SQLException se){
		      se.printStackTrace();
		   }catch(Exception e){
		      e.printStackTrace();
		   }
		 return conn;
	}
	public static void main(String[] args) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			connection.setAutoCommit(false);
			System.out.println("Department that doesn't having any employee");
			pstmt = connection.prepareStatement(Constants.Queries.DEPT_HAV_NO_EMPLOYEE);	
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1));
			}
			System.out.println("-----------------");
			System.out.println("Department having employee");
			System.out.println(Constants.Queries.DEPT_HAV_EMPLOYEE);

			pstmt = connection.prepareStatement(Constants.Queries.DEPT_HAV_EMPLOYEE);	
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1));
			}
			System.out.println("-----------------");
			
			System.out.println("-----------------");
			
			pstmt = connection.prepareStatement(Constants.Queries.UPDATE_EMPLOYEE_SALARY);
			pstmt.executeUpdate();
			pstmt = connection.prepareStatement(Constants.Queries.EMPLOYEE_OF_PARTICULAR_DEPT);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+ " "+ rs.getInt(2)+ " "+rs.getInt(3) );
			}
			System.out.println("-----------------");
			System.out.println("departments and their employess");

			pstmt = connection.prepareStatement(Constants.Queries.COUNT_EMPLOYEE_OF_DEPT);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+ " " + rs.getInt(2));
			}
			System.out.println("-----------------");
			System.out.println("employees belong to same city");
			pstmt = connection.prepareStatement(Constants.Queries.EMP_OF_SAME_CITY);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+ " " + rs.getString(2)+ " " +rs.getString(3));
			}
			connection.commit();
			
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}if(connection!=null){  
				connection.close();  
			}   
		}



}

}
