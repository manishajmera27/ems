package com.girnar;

abstract class example_abstract {
	example_abstract() {
		System.out.println("in abstract method");
	}

	public static void rectanglearea(int a, int b) {
		int area = a * b;
		System.out.println(area);
	}

}
